---
title: Home
---

<!--
{{< news >}}

### Good News!

I've been elected to a four year term in the Collegium of the Royal Academy of Belgium! Keep an eye out for new projects and collaborations.

{{< /news >}}
-->

# About Me

<img src="/images/pence.png" alt="Charles Pence" class="u-pull-right" style="max-width: 150px; margin: 1rem;">

I am a philosopher and historian of science and technology, working as Chargé de
cours (equiv. Associate Professor) in the [Institut supérieur de
philosophie](https://uclouvain.be/fr/instituts-recherche/isp), the [Louvain
Institute of Biomolecular Science and
Technology](https://uclouvain.be/en/research-institutes/libst) and the [Faculté
de philosophie, arts et lettres](https://uclouvain.be/fr/facultes/fial) at the
[Université catholique de Louvain (UCLouvain)](https://uclouvain.be), in
Louvain-la-Neuve, Belgium.

I am the director there of the [Centre de philosophie des sciences et sociétés
(Center for Philosophy of Science and Society, or
CEFISES)](https://cefises.be/en/). I am also a member of the [Collegium of the
Académie royale de
Belgique](https://academieroyale.be/fr/l-academie-royale-collegium-presentation-bureau/)
and the Executive Editor of the journal [_Philosophy, Theory, and Practice in
Biology_](https://www.ptpbio.org).

I hold an A.B. in philosophy from Princeton, and a Ph.D. in the history and
philosophy of science from the University of Notre Dame. My work has appeared
with leading university presses and journals, and I have spoken at a variety of
prestigious conferences on four continents. [You can find a copy of my CV
here,](https://archive.charlespence.net/cv.pdf) and my e-mail address is
<charles@charlespence.net>.

When I’m not working (which is fairly rare), I’m an avid marathon runner,
technology addict and computer programmer, clarinet player, gaming aficionado,
and audiophile. For more random facts about me, [click here…]({{< relref
"random.md" >}})

{{< leftcol >}}

## My research

I lead a committed group of scholars, working to understand the philosophy,
history, and ethics of science and technology. And we’re always looking for more
collaborators! Visit the lab website to learn more about our work.

<a href="https://pencelab.be" class="button button-primary">Learn more…</a>

{{< /leftcol >}}

{{< rightcol >}}

## My teaching

I teach a variety of courses at UCLouvain, to students from many disciplines. If
you’re one of my current students, or are interested in these subjects, click
below to learn more about what I am teaching now and have planned to teach in
upcoming semesters.

<a href="{{< ref "courses" >}}" class="button button-primary">My courses…</a>

{{< /rightcol >}}

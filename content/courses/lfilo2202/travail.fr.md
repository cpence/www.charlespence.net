---
title: "LFILO2202 : Travail écrit"
---

# LFILO2202 : Détails sur le travail écrit

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "agenda.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lfilo2202.php" class="button button-primary">Lecture</a>
<a href="{{< relref "travail.fr.md" >}}" class="button button-primary">Travail</a>

--------------------------------------------------------------------------------

Comme indiqué dans le programme général, le travail final sera rédigé par
étapes. Voici plus d’informations sur le fonctionnement de ce processus. Notez
que tous les devoirs ci-dessous peuvent être rédigés en français ou en anglais.

Certains sujets de travaux seront discutés au cours du semestre, mais il vous
incombera en fin de compte de choisir un sujet correspondant à vos intérêts.
*Les étudiants qui choisissent un sujet qu’ils aiment vraiment obtiennent
presque toujours de meilleures notes.* Prenez le temps de réfléchir (et de me
parler !) à la façon de relier notre matière à vos divers intérêts
philosophiques.

### Plan/Esquisse : 8 novembre {.syllabus-day}

<!-- Five weeks before the draft is due, or the last Sunday of fall break,
if that falls during fall break -->

Votre première tâche consiste à préparer un plan ou une esquisse de votre idée
de travail. Votre sujet peut être *n’importe quelle chose qui vous intéresse,*
pourvu qu’elle soit liée d’une manière ou d’une autre à une partie du matériel
que nous couvrons dans le cours. (Si vous n’êtes pas sûr, envoyez-moi un
courriel pour vérifier).

Il n’y a pas de format spécifique pour ce plan, car je sais que tout le monde a
des méthodes différentes pour préparer des travaux. L’exigence minimale est
d’avoir suffisamment de texte pour que cela représente environ **une page au
total** (qu’il s’agisse d’un “plan” ou simplement d’une esquisse de ce que vous
souhaitez faire, c’est à vous de voir), et d’avoir **au moins trois sources**
qui seront importantes pour votre argumentation.

N’hésitez pas, dans le cadre du plan, à me demander de l’aide, qu’il s’agisse de
trouver d’autres références, de compléter des parties de l’argumentation ou de
tout autre élément que vous souhaitez.

Je renverrai des commentaires sur ces plans *avant le 15 novembre.*
<!-- One week later, or Friday of the first full week back on campus after
fall break -->

### Ébauche à commenter : 13 décembre (ou plus tôt) {.syllabus-day}

<!-- End of week 12 (second-to-last week) -->

Si vous souhaitez que je vous donne des commentaires sur votre travail, vous
devez me soumettre une ébauche complète au plus tard le 13 décembre. Cela me
laissera suffisamment de temps pour rédiger des commentaires détaillés, vous les
renvoyer, puis vous donner suffisamment de temps pour pouvoir intégrer ces
commentaires.

Je renverrai des commentaires sur ces ébauches *avant le 27 décembre.*
<!-- Two weeks after the above due date -->

### Version finale : 16 janvier, 23h59 (ou plus tôt) {.syllabus-day}
<!-- Thursday of the second week of exam period -->

Je ne spécifie pas précisément la longueur requise pour ce travail, mais je
serais surpris que vous puissiez écrire un document de recherche de qualité, de
niveau master, en moins de 10 pages (interligne 1,5, police 12pt normale, etc.),
et je serais également surpris que vous ayez besoin de plus de 15 pages pour
exprimer votre argument.

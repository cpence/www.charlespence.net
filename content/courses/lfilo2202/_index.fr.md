---
title: "LFILO2202 : Éthique biomédicale"
---

# LFILO2202 : Éthique biomédicale

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "agenda.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lfilo2202.php" class="button button-primary">Lecture</a>
<a href="{{< relref "travail.fr.md" >}}" class="button button-primary">Travail</a>

*Site web pour Q1, 2024-2025 ; dernière mise à jour le 3 septembre, 2024*

--------------------------------------------------------------------------------

**Professeur :** Charles H. Pence\
**Horaire :** mercredi, 10h45-12h45, ERAS 75\
**Date d’examen mi-parcours :** 13 novembre (à l’heure et la salle normale du cours)\
**Date de remise de travail :** 16 janvier, 23h59\
**Autres détails :** 5.0 crédits, 30.0h

### Thèmes abordés

Une première partie du cours décrit les grandes lignes du contexte philosophique
et biomédical de l’éthique biomédicale contemporaine. Quelques théories et
concepts fondamentaux sont analysés. A partir de là, diverses questions
concrètes sont étudiées dans divers domaines de la pratique biomédicale :
procréation, fin de vie, politique des soins de santé, etc.

### Contenu

Ce cours est une introduction à la discussion contemporaine sur l’éthique
biomédicale, y compris l’éthique de la pratique de la médecine et l’éthique de
la recherche médicale et biochimique. On abordera les concepts classiques de
l’éthique médicale (confidentialité, paternalisme, autonomie) et la recherche
biomédicale (consentement éclairé, compétence des patients). Ces discussions
seront encadrées par quelques exemples, aussi classiques (euthanasie, IVG) que
modernes (cellules souches, édition génétique). Quelques séances seront aussi
dédiées aux questions de la relation entre médecine et société (distribution des
ressources de santé, production des nouveaux médicaments). Le cours est
accessible aux étudiant·e·s quel que soit leur niveau en sciences de la vie ; on
va présenter et discuter des fondements requis de la biologie contemporaine.

Le travail des étudiant·e·s en fin du cours va approfondir un cas d’étude dans
le monde contemporain de santé et soulever, discuter, et évaluer ses dimensions
éthiques.

### Acquis d’apprentissage

A la fin de cette unité d’enseignement, l’étudiant est capable de :

1.  d’identifier et d’approfondir les théories et concepts constituant les
    divers courants d’éthique biomédicale ;
2.  de traiter en profondeur un problème particulier d’éthique biomédicale ;
3.  d’en saisir les dimensions interdisciplinaires ;
4.  de mettre en relation les données du problème avec des développements
    scientifiques contemporains ;
5.  de distinguer une proposition de nature philosophique d’une proposition
    d’une autre nature (médicale, juridique, …)

## Lecture

Tout de la lecture sera distribué électroniquement [dans le site web
« readings ».](https://readings.charlespence.net/lfilo2202.php) Le mot de passe
pour ce site sera distribué par courriel avant le début du cours et pendant la
première séance.

## Évaluation

-   **Examen mi-parcours (40 %) :** Une fois qu’on a terminé avec la première
    partie du cours (avant qu’on ne tourne vers la biotechnologie), on aura un
    examen « mi-parcours » qui couvre les bases philosophiques importants, afin
    de vous préparer d’attaquer à votre sujet pour le travail. Vous trouverez
    [des questions de
    révision](https://archive.charlespence.net/courses/lfilo2202/questions-revision.pdf)
    ici pour cet examen. L’examen sera des questions courtes inspirées par ces
    questions et thèmes.

-   **Travail final écrit (60 %) :** Le principal résultat de ce cours sera un
    travail écrit. Nous construirons cet article par étapes, en commençant par
    un bref aperçu à rendre vers le milieu du quadrimestre, suivi d’une ébauche
    sur laquelle je ferai des commentaires et d’une version finale à la fin du
    quadri.

Dans la session d’août, l’évaluation consiste d’un examen écrit (100 %).

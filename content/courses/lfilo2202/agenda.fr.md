---
title: "LFILO2202 : Agenda"
---

# LFILO2202 : Agenda

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "agenda.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lfilo2202.php" class="button button-primary">Lecture</a>
<a href="{{< relref "travail.fr.md" >}}" class="button button-primary">Travail</a>

--------------------------------------------------------------------------------

### 18 septembre : Qu’est-ce que la bioéthique ? Quelles théories générales pourrons-nous utiliser afin de prendre des décisions éthiques ? {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine1.pdf)

Quelles sont les relations entre la bioéthique, l’éthique médicale, l’éthique
appliquée, et l’éthique normative ? Comment comprendre le statut de la
bioéthique, en tant qu’une discipline positionnée (paradoxalement ?) entre la
pratique médicale et scientifique et la philosophie ?

#### Lecture

-   Saint-Arnaud (1999), « Qu’est-ce que la bioéthique ? »
-   Keeling et Bellefleur (2016), « Le « principisme » et les cadres de
    référence en matière d’éthique en santé publique »
-   Benaroyo (2010), « L’émergence de la bioéthique » (*facultatif*)
-   Beauchamp and Childress (2013), ch. 1, “Moral Foundations” (*facultatif,
    mais fortement recommandé pour les fondements du principisme*)
-   Tong (2002), “Teaching Bioethics in the New Millennium” (*facultatif, mais
    recommandé pour une critique très fort du principisme*)

### 25 septembre : Qu’est-ce que la santé ? La maladie ? La pathologie ? {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine2.pdf)

Le cible de la médecine est *la santé* ou *la guérison.* Mais au moins depuis le
travail de Georges Canguilhem, on se rend compte que ces buts nécessitent une
définition d’en quoi consiste exactement la santé, la maladie, la normalité, ou
la pathologie. Comment peut-on définir ces notions, prenant en compte les
souhaits et la perspective des patients ?

#### Lecture

-   Wakefield (1992), « Le concept de trouble mental »
-   Boorse (1977), « Le concept théorique de santé » (*facultatif, mais
    recommandé pour l’argument contre Wakefield*)
-   Hesslow (1993), « Avons-nous besoin d’un concept de maladie ? »
    (*facultatif*)

### 2 octobre : Confidentialité, divulgation, paternalisme {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine3.pdf)

Les médecins confrontent quotidiennement des questions de confidentialité et
divulgation. Doit-on dire la vérité toujours aux patients ? *L’entière* vérité ?
Quand devrait-on divulguer des renseignements aux tiers, et comment ? C’est
clair que le médecin a de la connaissance spécialiste (c’est pourquoi le
patient·e lui consulte), mais dans quelles circonstances devrait-on donner le
dernier mot au médecin ?

#### Lecture

-   Jaunait (2003), « Comment peut-on être paternaliste ? »
-   *Études de cas :*
    -   Divulgation des renseignements médicaux (Constantine, Collège Royal)
    -   Dire la vérité au patient (Callanan, Collège Royal)
    -   Le signalement de patients pouvant causer préjudice à autrui (sécurité
        publique) (McRae, Collège Royal)

### 9 octobre : Autonomie, consentement éclairé, compétence des patients, et euthanasie {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine4.pdf)

Comment les patients peuvent-ils comprendre assez de détails sur leur condition
et traitement afin d’offrir un consentement éclairé, s’ils ne sont pas de
docteurs ? Comment assurer qu’ils réfléchissent sur les options et ne pas tout
simplement accepter ce qu’est leur dit ? Comment prendre en compte les
directives anticipées en fin de vie, y compris la question de l’euthanasie ?

#### Lecture

-   Beauchamp et Childress (2007), ch. 3, « Le respect de l’autonomie »
    (*extrait*)
-   Saint-Arnaud (1999), « L’euthanasie : des équivoques à dissiper »
-   Mortier (2003), « Directives anticipées : origines, utilité, fondements et
    controverses » (*facultatif*)
-   *Études de cas :*
    -   Capacité de décision (Charland et Lachmann, Collège Royal)
    -   L’avortement et la capacité à consentir (Sobsey et al., Collège Royal)
    -   Rapport 2018-2019 sur l’euthanasie en Belgique (Institut Européen de
        bioéthique)

### 16 octobre : Le handicap et la philosophie de *disability* {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine5.pdf)

Qu’est-ce qu’on fait quand la médecine ne peut pas cibler la guérison totale
d’un·e patient·e ? Dans les cas de l’handicap ou la maladie chronique, doit-on
repenser les fondements de la médecine afin de pouvoir prendre en compte des
maladies qui ne se guérit pas (ou même qui ne doivent pas être considérés comme
des maladies) ?

#### Lecture

-   Moyse (2010), « Le concept de personne dans le champ du handicap »
-   Marin (2010), « La maladie chronique ou le temps douloureux »
-   Fins (2023), “The complicated legacy of Terry Wallis and his brain injury”
    (*facultatif mais recommandé*)
-   Terzi (2004), “The social model of disability: a philosophical critique”
    (*facultatif*)
-   Wendell (2001), “Unhealthy disabled: treating chronic illnesses as
    disabilities” (*facultatif*)

<!--

Removed from right here to accommodate PSA in New Orleans

### 27 octobre : L'éthique biomédicale interculturelle {.syllabus-day}

Comment nos perspectives sur l'éthique biomédicale se changent dès qu'on regarde
autres cultures que celle de l'Europe occidentale ? On tentera d'aborder les
questions de santé, vie, et mort par le biais des approches musulmanes,
africaines, et chinoises.

#### Lecture

- Le Breton (2010), « Éthique des soins en situation interculturelle à
  l'hôpital »
- _selon vos intérêts, choisir au moins un des suivants :_
  - Ouédraogo (2010), « Approche philosophique de la maladie en Afrique noire »
  - Aggoun (2010), « Les musulmans, leur corps, la maladie et la mort »
  - Guo (1995), "Chinese Confucian culture and the medical ethical tradition"
  - Qiu (1993), "Chinese medical ethics and euthanasia"

-->

### 23 octobre : La distribution des ressources de santé et la justice sociale {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine7.pdf)

Les services de santé sont évidemment une ressource rare : ce n’est pas le cas,
en général, que chaque personne qui en est éligible peut obtenir n’importe quel
traitement de n’importe quelle maladie qu’ils ont, quoi que soient les coûts.
Comment distribuer ces ressources ? Comment exprimer en pratique le droit à une
qualité de soins basique ?

Aussi, à la fin de cette session on va discuter de vos travaux écrits, y compris
quelques consignes à suivre et pièges à éviter.

#### Lecture

-   Pelluchon (2009), « Justice distributive et solidarité » (*extrait*)
-   de Kervasdoué (2010), « Économie et gestion de la santé : l’argent des
    autres » (*facultatif*)
-   Saint-Arnaud (1999), « L’egalité d’accès aux soins en situation de rareté »
    (*facultatif*)
-   de Singly (2010), « Le « soin juste » sous contrainte économique à
    l’hôpital » (*facultatif*)
-   *Études de cas :*
    -   Délais d’attente (Clarke, Collège Royal)

### 30 octobre : (pas de cours, vacances) {.syllabus-day}

### 6 novembre : Éthique de la recherche et les essais cliniques {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine8.pdf)

Comment les nouveaux médicaments arrivent-ils sur la marche ? Quelles
obligations éthiques trouve-t-on dans le processus de la recherche biomédicale ?
Comment évaluer le système d’essais cliniques contemporain ?

#### Lecture

-   Dumitru et Leplège (2010), « La course aux brevets dans la médecine
    personnalisée : une étude de cas »
-   Mary (2021), « Essais cliniques : l’éthique face à l’innovation »
-   Halioua (2010), « Du procès au Code de Nuremberg : principes de l’éthique
    biomédicale » (*facultatif*)

### 13 novembre : **examen mi-parcours** {.syllabus-day}

**Pr. Pence à l’étranger pour un congrès**

Notre examen mi-parcours sera pendant les deux heures du cours de cette journée.

### 20 novembre : Cours accéléré : la génétique, les cellules souches, le clonage, l’édition génétique, etc. {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine9.pdf)

On terminera notre cours avec quelques semaines de discussion des impacts
éthiques des nouvelles biotechnologies. Tout d’abord, il faut être en mesure de
discuter les fondements biologiques de l’ADN et les techniques laboratoires qui
nous permettent (au moins éventuellement) d’améliorer nos traitements médicaux.

#### Lecture

-   Hirsch (2010), « Les biotechnologies, un nouveau paradigme de la recherche »

-   *Études de cas :*

    -   Les considérations éthiques liées au diagnostic génétique
        présymptomatique (Mezer et Levin, Collège Royal)

-   *Vidéos:*

    Les vidéos qui suivent sont arrangées dans l’ordre de difficulté, d’un
    niveau très introductif jusqu’à quelques techniques assez récentes de la
    biotechnologie. Si vous voulez de l’approfondissement et vous êtes en mesure
    de suivre quelques vidéos en anglais, je conseille fortement la série
    [Learn.Genetics.](https://learn.genetics.utah.edu/)

    1.  [Qu’est-ce qu’une cellule ? —
        Inserm](https://www.youtube.com/watch?v=VDEfZh9lmi0)
    2.  [Le noyau — Inserm](https://www.youtube.com/watch?v=qlxOcQExuF8)
    3.  [La transcription de l’ADN —
        Inserm](https://www.youtube.com/watch?v=-YXUon94a8Y)
    4.  [Qu’est-ce qu’une cellule souche ? —
        Inserm](https://www.youtube.com/watch?v=TBvgGrXuqcY)
    5.  [À quoi servent les cellules souches ? —
        Inserm](https://www.youtube.com/watch?v=pvtY09DkNZY)
    6.  [La reprogrammation cellulaire : les principes —
        Inserm](https://www.youtube.com/watch?v=gh-ckXhm_co)
    7.  [La reprogrammation cellulaire au labo —
        Inserm](https://www.youtube.com/watch?v=S-uCPluxwtM)
    8.  [Introduire un gène humain dans une bactérie —
        GeneABC](https://www.youtube.com/watch?v=VLGtvhi0TCo)
    9.  [Les souris transgéniques —
        GeneABC](https://www.youtube.com/watch?v=FkY-pnkT3uU)
    10. [CRISPR/CAS9 : une méthode révolutionnaire —
        Inserm](https://www.youtube.com/watch?v=RplWR12npqM)

### 27 novembre : Édition génétique chez l’être humain : thérapie, amélioration, et eugénisme {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine10.pdf)

Bien que la possibilité de la thérapie génique soit déjà une réalité pour
quelques maladies, des nouvelles techniques comme le CRISPR-Cas9 promettent
d’étendre l’ampleur et la fréquence d’utilisation de ces techniques. Quels
enjeux éthiques sont soulevés ? Y aura-t-il la possibilité d’un nouvel eugénisme
génétique ?

#### Lecture

-   Hirsch (2018), « CRISPR : lorsque modifier le génome devient possible »
-   Morange (2001), « L’eugénisme aujourd’hui »
-   Goffette et Jacobi (2006), « Discours eugéniste d’hier et discours
    “eugéniste” d’aujourd’hui : les limites d’une comparaison » (*facultatif*)
-   Briard (2010), « Approche éthique de la génétique » (*facultatif*)
-   *Études de cas :*
    -   La recherche sur les thérapies géniques et les droits des enfants
        (Samuël et Tremblay, Collège Royal)

### 4 décembre : Clonage {#décembre-clonage .syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine11.pdf)

Il y a déjà presque 25 ans que le premier mouton (et premier mammifère),
« Dolly », était cloné. Pour autant que l’on ne soit pas beaucoup plus proche au
clonage humain, les mêmes techniques sont utilisées dans la médecine
reproductive et d’autres parties de la thérapie génique.

#### Lecture

-   Brock (1997), “Cloning human beings: an assessment of the ethical issues pro
    and con”
-   Atlan (2002), « De la biologie à l’éthique : Le « clonage » thérapeutique »
-   Jouneau et Renard (2002), « Cellules souches embryonnaires et clonage
    thérapeutique » (*facultatif, très technique*)
-   *Études de cas :*
    -   Le clonage humain (Isasi et Shukairy, Collège Royal)

### 11 décembre : La biologie synthétique, organismes transgéniques, et organoïdes {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine12.pdf)

Tout à la frontière de la recherche biologique, les chercheurs essaient de
produire nouveaux systèmes biologiques, afin de reproduire des comportements
plus complexes *in vitro* et de comprendre les processus cellulaires en leur
construisant. Quels sont les problèmes éthiques soulevés par des cellules,
organes, ou même la vie « artificielle » ?

#### Lecture

-   Flocco (2018), « Points de vue éthiques sur la biologie de synthèse : la «
    marche du progrès » en question »
-   Comité d’éthique de l’Inserm (2020), « La recherche sur les organoïdes :
    quels enjeux éthiques ? »
-   Rozenbaum (2019), « Organoïdes : quelle place dans la recherche de
    demain ? »

### 18 décembre : Thérapie, amélioration, et transhumanisme {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lfilo2202/semaine13.pdf)

L’amélioration du corps ou de l’esprit humain (“human enhancement”) se trouve
comme un cousin gênant de la pratique médicale. D’un côté, la médecine fait déjà
des pratiques qui semble « améliorer » l’être humain (vaccins, lunettes), meme
si à ses limites, le transhumanisme cherche dépasser totalement les bornes de la
médecine traditionnelle. Qu’est-ce que la distinction entre thérapie et
amélioration ? Quels problèmes éthiques suivront des éventuelles améliorations
des êtres humains ?

#### Lecture

-   Baertschi (2011), « Qu’est-ce qu’une véritable amélioration ? »
-   Hottois (2018), « Bioéthique, technosciences et transhumanisme »
-   McGee (2020), “Using the therapy and enhancement distinction in law and
    policy” (*facultatif*)
-   [Goffi (2017),
    « Transhumanisme »](https://encyclo-philo.fr/transhumanisme-a)
    (*facultatif*)

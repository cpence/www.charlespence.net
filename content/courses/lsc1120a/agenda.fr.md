---
title: "LSC1120A: Agenda"
---

# LSC1120A : Agenda

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "agenda.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lsc1120a.php" class="button button-primary">Lecture</a>

-----

### 17 septembre : Introduction : Les valeurs éthiques et la pratique de la science {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine1.pdf)

#### Lecture obligatoire

Trois lettres sur le développement des armes nucléaires : Einstein et Szilárd au
Président Roosevelt ; demande de signer une pétition de Szilárd à Teller ;
réponse de Teller à Szilárd.

#### Autres ressources

- Coutellec, Léo. 2013. « Une réévaluation du pluralisme axiologique ».

### 24 septembre : L’agnotologie et les « marchands de doute » {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine2.pdf)

#### Lecture obligatoire

Girel, Mathias. 2013. « Agnotologie : mode d’emploi ».

#### Autres ressources

- Oreskes, Naomi and Erik M. Conway. 2012. « Le déni du réchauffement
  climatique ». Extrait de _Les marchands de doute._
- Proctor, Robert. 2008. “Agnotology: A Missing Term to Describe the Cultural
  Production of Ignorance (and its Study).”

### 1 octobre : Le « _Big Data_ » et les sciences {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine3.pdf)

#### Lecture obligatoire

Huneman, Philippe. 2023. « Signatures et prédiction ? Le tournant épistémique
des données massives ».  
(pages 47–61 obligatoires, le reste facultatif)

#### Autres ressources

- Anderson, Chris. 2008. “The End of Theory: The Data Deluge Makes the
  Scientific Method Obsolete.”
- boyd, danah and Kate Crawford. 2012. “Critical Questions for Big Data.”

### 8 octobre : Révolutions scientifiques : la révolution chimique {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine4.pdf)

#### Lecture obligatoire

Mazauric, Simone. 2009. « Lavoisier et la révolution chimique ». Extrait de
_Histoire des sciences à l’époque moderne._  
(pages 1, 8–17 obligatoires, le reste facultatif)

#### Autres ressources

- Bensaude-Vincent, Bernadette et Isabelle Stengers. 1993. _Histoire de la
  chimie_ (extrait).
- Kuhn, T. S. 1983. _La structure des révolutions scientifiques_ (extraits).

### 15 octobre : La science en Chine et le monde Arabe {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine5.pdf)

#### Lecture obligatoire

Bonnet-Bidaud, Jean-Marc. 2013. « La science chinoise ».  
**ou**  
Weinberg, Achille. 2013. « L’apport du monde arabe ».

### 22 octobre : Les sciences historiques et les science expérimentales {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine6.pdf)

#### Lecture obligatoire

Bourguet, Marie-Noëlle et Pierre-Yves Lacour. 2015. « Les mondes naturalistes :
Europe (1530-1802) ». Extrait de _Histoire des sciences et des savoirs (I)._  
(pages 255-256, 262-267 obligatoires, le reste facultatif)

#### Autres ressources

- Cleland, Carol. 2001. “Historical Science, Experimental Science, and the
  Scientific Method.”

### 29 octobre : (pas de cours, vacances) {.syllabus-day}

<!-- 

### 5 novembre : Idéalisation et abstraction dans les mathématiques {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine7.pdf)

#### Lecture obligatoire

Bravo, Felipe. 2017. « Platonisme mathématique ».
https://encyclo-philo.fr/platonisme-mathematique-gp

#### Autres ressources

- Nahin, Paul. 1998. “The Puzzles of Imaginary Numbers.” Excerpt from _An
Imaginary Tale: The Story of √-1._

-->

### 5 novembre : La spéculation théorique en physique {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine8.pdf)

#### Lecture obligatoire

Davies, Paul. 2007. « Comment visiter le futur ». Extrait de _Comment construire
une machine à explorer le temps ?_

#### Autres ressources

- Gott, J. R. 2001. “Time Travel to the Past: Cosmic Strings.” From _Time Travel
  in Einstein’s Universe._
- Deutsch, David and Michael Lockwood. 1994. “The Quantum Physics of Time
  Travel.”
- Lewis, David. 1976. “The Paradoxes of Time Travel.”

### 12 novembre : **pas de cours !** {.syllabus-day}

**Pr. Pence à l’étranger pour un congrès**

On aura peut-être un cours en forme vidéo qui remplacera cette journée ; je vous
tiens au courant.

### 19 novembre : Modélisation et simulation numérique {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine9.pdf)

#### Lecture obligatoire

Barberousse, Anouk. 2008. « Les simulations numériques de l’évolution du
climat : de nouveaux problèmes philosophiques ? ».

#### Autres ressources

- Varenne, Franck. 2012. « Modèle et réalité ». Extrait de _Théorie, réalité,
  modèle._

### 26 novembre : La science et la pseudo-science {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine10.pdf)

#### Lecture obligatoire

Debray, Stéphanie. 2020. « Pseudoscience ».
https://encyclo-philo.fr/pseudoscience-gp

#### Autres ressources

- Hansson, Sven Ove. 2021. “Science and Pseudo-Science.”
  https://plato.stanford.edu/entries/pseudo-science/
- Pigliucci, Massimo, 2015. “Scientism and Pseudoscience: A Philosophical
  Commentary.”

### 3 décembre : Science et capitalisme {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine11.pdf)

#### Lecture obligatoire

Laval, Christian, Francis Vergne, Pierre Clément, et Guy Dreux. 2011. « Le
régime néolibéral de la connaissance ». Extrait de _La nouvelle école
capitaliste._  
(pages 55-59 et 65-68 obligatoires, pages 74-80 recommandées, le reste
facultatif)

#### Autres ressources

- Mirowski, Philip. 2012. “The Modern Commercialization of Science is a Passel
  of Ponzi Schemes.”
- Mirowski, Philip and Robert van Horn. 2005. “The Contract Research
  Organization and the Commercialization of Scientific Research.”

### 10 décembre : La philosophie féministe de la science  {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine12.pdf)

#### Lecture obligatoire

Pestré, Dominique. 2006. « Femmes, genre et science : objectivité et parti
pris ». Extrait de _Introduction aux_ Science Studies.

#### Autres ressources

- Richardson, Sarah. 2008. “When Gender Criticism Becomes Standard Practice: The
  Case of Sex Determination Genetics.”
- Okruhlik, Kathleen. 1994. “Gender and the Biological Sciences.”

### 17 décembre : Philosophie de la technologie {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120a/semaine13.pdf)

#### Lecture obligatoire

Winner, Langdon. 1980. « Les artefacts font-ils de la politique ? »  
(pages 45–54, 59–61, et 71–72 obligatoires, le reste facultatif)

#### Autres ressources

- Latour, Bruno. 2011. “Love Your Monsters: Why We Must Care for our
  Technologies as We Do Our Children.”
- Lancelot, Mathilde. 2019. « Technologie ».
  https://encyclo-philo.fr/technologie-gp


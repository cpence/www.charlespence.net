---
title: "LSC1120A : Notions de philosophie"
---

# LSC1120A : Notions de philosophie

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "agenda.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lsc1120a.php" class="button button-primary">Lecture</a>

_Site web pour Q1, 2024–2025; dernière mise à jour le 4 septembre, 2024_

---

**Professeur:** Charles H. Pence  
**Horaire:** Mardi, 16h15-18h15, MONT 11 ou MONT 10 (**sauf** la première séance, 17-19h)  
**Examen final:** (à déterminer)  
**Autres détails:** 2 crédits, 30.0h

## Contenu

Ce cours va explorer des thèmes centraux dans la philosophie et l’histoire des
sciences, l’épistémologie, et l’éthique des sciences. Parmi d’autres, on va
étudier la génération et la structure de la connaissance scientifique, les
relations entre la science et notre société, les pratiques scientifiques (comme
la modélisation ou l’utilisation du « big data »), et le rapport
science-technologie.

## Partie VETE

**À noter :** Ce site est destiné à tou·te·s les étudiant·e·s inscrit·e·s dans
LSC1120. Ce cours comprend aussi une autre partie, LSC1120B, qui ne concerne que
les étudiant·e·s dans le programme VETE. [Cliquez ici pour le site de
LSC1120B.]({{< relref "../lsc1120b" >}})

## Évaluation

L'évaluation prendra la forme d'un examen QCM, avec quelques questions écrites
très courtes (1–2 phrases max.).

Dans la session d’août, l’évaluation consiste d’un examen écrit.

## Enseignement

Ce cours sera normalement un cours magistral classique ; je présenterai les
concepts, problèmes, et questions d'interprétation classiques de la philosophie
des sciences, appliquée autant que possible à votre parcours de scientifique. Je
vous encourage de :

1. Lire attentivement la lecture requise et, si vous avez le temps ou l'intérêt,
   peut-être voir quelques vidéos ou lire quelques « autres ressources » pour la
   session.
2. Arriver au cours avec des questions ou points de clarification. Quels sont
   les points les plus intéressants ou les moins claires de la lecture ? Si ces
   questions ne sont pas résolues pendant ma discussion, n'hésitez pas de les
   poser !

<!--
**Toutes les sessions de ce cours seront disponible comme des vidéos, vu que
certain·e·s étudiant·e·s sont en situation de conflit horaire.** Ces videos
seront disponibles sur notre site Moodle.
-->

## Lecture

Toute la lecture pour ce cours se trouvera dans [mon propre site
« readings ».](https://readings.charlespence.net) Ce site est protégé par un mot
de passe, qui sera distribué par courriel et lors de notre première séance de
cours.

## Comment réviser

Quelques étudiant·e·s m’ont demandé comment on doit mieux étudier/réviser la
matière pour ce cours. Les ressources les plus importantes seront les slides et
la lecture. Essayer de vous demander :

- Quel était le but de cette semaine ? Quelle était le leçon par rapport à la
  science qu’on a ciblé ?
- Comment la lecture pour cette semaine est-elle liée à ce but ? Qu’est-ce que
  le lien entre cette lecture et le cours en général ?

Si vous avez toujours des doutes par rapport à ces questions, vous pouvez (1)
consulter les enregistrements pour chaque cours, et/ou (2) m’envoyer un mail ou
prendre un rendez-vous.

En tout cas, il ne sera *jamais* demandé que vous mémorisez des dates, noms, ou
citations exactes.

---
title: Enseignement
---

# Enseignement

**Étudiant·e·s :** Presque toutes les infos sur vos cours seront disponibles
ici, y compris les lectures au format PDF. S'il manque un lien pour un cours,
alors vous pourrez parfois trouver les informations sur Moodle.

{{< courselist >}}

## Ressources en philosophie des sciences

Je maintiens également une collection [d’autres ressources en philosophie des
sciences](./phil-sci-resources.md) pour les étudiant·e·s qui souhaitent
approfondir leurs connaissances.

---
title: Philosophy of Science Resources
---

# Philosophy of Science Resources

Here are a variety of optional readings, textbooks, and resources that cover the
kind of material that I teach in my philosophy of science classes (LFILO1220,
LFILO2602, etc.). More difficult items are marked with a ★ (but are still
recommended!).

All the items here are linked to their respective pages at WorldCat, which
should show their availability at any library near you (make sure to search by
title, though; there may be another edition to which you have access!). If you
want a book and can't wait for inter-library loan, e-mail me -- I might have a
copy!

## English

- General works on philosophy of science (classics, textbooks, and anthologies
  of important readings):
  - ★ [Balashov and Rosenberg, _Philosophy of Science: Contemporary
    Readings_](https://search.worldcat.org/title/46937540)
  - [Alexander Bird, _Philosophy of
    Science_](https://search.worldcat.org/title/894649603)
  - [David Boersema, _Philosophy of Science (Text with
    Readings)_](https://search.worldcat.org/title/191763352)
  - [Lisa Bortolotti, _An Introduction to the Philosophy of
    Science_](https://search.worldcat.org/title/934392381)
  - [A.F. Chalmers, _What Is This Thing Called
    Science?_](https://search.worldcat.org/title/41269448)
  - [Steven French, _Science: Key Concepts in
    Philosophy_](https://search.worldcat.org/title/940594187)
  - ★ [Ian Hacking, _Representing and Intervening: Introductory Topics in the
    Philosophy of Science_](https://search.worldcat.org/title/9371160)
  - [Peter Godfrey-Smith, *Theory and
    Reality*](https://search.worldcat.org/title/837729860)
  - [Thomas Kuhn, *The Structure of Scientific
    Revolutions*](https://search.worldcat.org/title/93075)
  - [James Ladyman, _Understanding Philosophy of
    Science_](https://search.worldcat.org/title/47658932)
  - [John Losee, _A Historical Introduction to the Philosophy of
    Science_](https://search.worldcat.org/title/6639969)
  - ★ [Machamer and Silberstein, _The Blackwell Guide to the Philosophy of
    Science_](https://search.worldcat.org/title/50661258)
  - ★ [W.H. Newton-Smith, _A Companion to the Philosophy of
    Science_](https://search.worldcat.org/title/777885608)
  - [Samir Okasha, *Philosophy of Science: A Very Short
    Introduction*](https://search.worldcat.org/title/48932644)
  - ★ [Psillos and Curd, _The Routledge Companion to the Philosophy of
    Science_](https://search.worldcat.org/title/930963732)
  - [Alex Rosenberg, *The Philosophy of Science: A Contemporary
    Introduction*](https://search.worldcat.org/title/880404023)
- Science and values:
  - [Heather Douglas, "Inductive Risk and Values in Science," *Philosophy of
    Science,* vol. 67, no. 4 (2000), pp.
    559–579](https://www.jstor.org/stable/188707)
  - ★ [Heather Douglas, *Science, Policy, and the Value-Free
    Ideal*](https://search.worldcat.org/title/938397598)
  - [Proctor and Schiebinger, eds., *Agnotology: The Making and Unmaking of
    Ignorance*](https://search.worldcat.org/title/913038912)
- The scientific revolution:
  - [David Lindberg, *The Beginnings of Western
    Science*](https://search.worldcat.org/title/756505913)
  - ★ [G.E.R. Lloyd, *Early Greek Science: Thales to
    Aristotle*](https://search.worldcat.org/title/1004975908)
  - ★ [H.F. Cohen, *The Scientific
    Revolution*](https://search.worldcat.org/title/793190489)
  - [Steven Shapin, *The Scientific
    Revolution*](https://search.worldcat.org/title/1022777492)
- The chemical revolution:
  - ★ [William Brock, *The Norton History of
    Chemistry*](https://search.worldcat.org/title/919648522)
  - [Trevor Levere, *Transforming
    Matter*](https://search.worldcat.org/title/697779494)
- The Darwinian revolution:
  - [Peter J. Bowler, *Charles
    Darwin*](https://search.worldcat.org/title/731572144)
  - ★ [Janet Browne, *Charles Darwin: A
    Biography*](https://search.worldcat.org/title/34046206) (2 vols.)
  - [Desmond and Moore, *Darwin's Sacred Cause: How a Hatred of Slavery Shaped
    Darwin's Views on Human
    Evolution*](https://search.worldcat.org/title/839689475)
  - ★ [Peter J. Bowler, *The Eclipse of
    Darwinism*](https://search.worldcat.org/title/228665727)
- The Einsteinian revolution:
  - [Nick Huggett, *Space from Zeno to Einstein: Classic Readings with a
    Contemporary Commentary*](https://search.worldcat.org/title/615304930)
  - ★ [Lawrence Sklar, *Space, Time, and
    Spacetime*](https://search.worldcat.org/title/837691002)
- Models and objectivity:
  - [*Models and Simulations 1,* a special issue of
    *Synthese*](http://link.springer.com/journal/11229/169/3/page/1)
  - [*Models and Simulations 2,* a special issue of
    *Synthese*](http://link.springer.com/journal/11229/180/1/page/1)
  - ★ [Lorraine Daston and Peter Galison,
    *Objectivity*](https://search.worldcat.org/title/913067824)
  - [Eric Winsberg, *Science in the Age of Computer
    Simulation*](https://search.worldcat.org/title/587198809)
- Sociobiology, evolutionary psychology, and design:
  - ★ [Philip Kitcher, *Vaulting Ambition: Sociobiology and the Quest for Human
    Nature*](https://search.worldcat.org/title/308197678)
  - [Edward Larson, *Summer for the Gods: The Scopes Trial and America's
    Continuing Debate over Science and
    Religion*](https://search.worldcat.org/title/755929778)
  - [Robert Pennock and Michael Ruse, *But is it Science? The Philosophical
    Question in the Creation/Evolution
    Controversy*](https://search.worldcat.org/title/275151315)

## French

- Travaux généraux de la philosophie des sciences (classiques, manuels, et
  collections des textes importants) :
  - [Barberousse, Bonnay, et Cozic, _Précis de philosophie des
    sciences_](https://search.worldcat.org/title/933427026)
  - [Barberousse, Kistler, et Ludwig, _La philosophie des sciences au XXe
    siècle_](https://search.worldcat.org/title/1255623785)
  - ★ [Michel Bitbol et Jean Gayon, _L’épistémologie française,
    1830–1970_](https://search.worldcat.org/title/910854879)
  - ★ [Braunstein, Diez, et Vagelli, _L’épistémologie historique : histoire et
    méthodes_](https://search.worldcat.org/title/1124074326)
  - [Michael Esfeld, _Philosophie des sciences : une
    introduction_](https://search.worldcat.org/title/1350736200)
  - [Vincent Israel-Jost, _L’Observation scientifique : aspects philosophiques
    et pratiques_](https://search.worldcat.org/title/913854154)
  - [Julie Jebeile, _Épistémologie des modèles et des simulations numériques :
    de la représentation à la compréhension
    scientifique_](https://search.worldcat.org/title/1129097688)
  - ★ [S. Laugier et P. Wagner, _Philosophie des sciences : naturalismes et
    réalismes_](https://search.worldcat.org/title/492599873)
  - ★ [S. Laugier et P. Wagner, _Philosophie des sciences : théories,
    expériences et méthodes_](https://search.worldcat.org/title/1252693433)
  - ★ [Jean Leroux, _Une histoire comparée de la philosophie des
    sciences_](https://search.worldcat.org/title/758621571) (2 tomes)
  - [Lena Soler, _Introduction à
    l’épistémologie](https://search.worldcat.org/title/1090532302)
- La science et les valeurs éthiques :
  - [Léo Coutellec, _De la démocratie dans les sciences : épistémologie, éthique
    et pluralisme_](https://search.worldcat.org/title/892896537)
  - [Dominique Pestré, _Introduction aux *Science
    Studies*_](https://search.worldcat.org/title/421320915)
  - [Prud’homme, Doray, et Bouchard, _Sciences, technologies et sociétés de A à
    Z_](https://search.worldcat.org/title/1191234121)
- L’histoire de la science :
  - [Bonneuil et Pestre, _Histoire des sciences et des
    savoirs_](https://search.worldcat.org/title/927449239) (3 tomes)
  - [Gingras, Keating, et Limoges, _Du scribe au savant : Les porteurs du savoir
    de l’Antiquité à la révolution
    industrielle_](https://search.worldcat.org/title/45700756)
  - [Simone Mazauric, _Histoire des sciences à l’époque
    moderne_](https://search.worldcat.org/title/1153441588)
- La révolution chimique :
  - ★ [Bensaude-Vincent et Stengers, _Histoire de la
    chimie_](https://search.worldcat.org/title/48113199)
- La révolution darwinienne :
  - [Heams, Huneman, Lecointre, et Silberstein, _Les mondes darwiniens :
    l’évolution de l’évolution_](https://search.worldcat.org/title/1269615253)
  - [Hoquet et Merlin, _Précis de philosophie de la
    biologie_](https://search.worldcat.org/title/898029768)
- Modèles et objectivité :
  - ★ [Franck Varenne, _Théorie, réalité,
    modèle](https://search.worldcat.org/title/877044003)
  - [Vincent Israel-Jost,
    _Objectivité(s)_](https://search.worldcat.org/title/1246633197)



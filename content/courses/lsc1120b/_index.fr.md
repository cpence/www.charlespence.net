---
title: "LSC1120B : Notions de philosophie (partie VETE)"
---

# LSC1120B : Notions de philosophie, d’éthologie, et d’éthique (partie VETE)

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "agenda.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lsc1120b.php" class="button button-primary">Lecture</a>
<a href="https://archive.charlespence.net/courses/lsc1120b/questions-revision.pdf"
class="button button-primary">Questions de Révision</a>

*Site web pour Q1, 2024-2025 ; dernière mise à jour le 8 septembre 2024*

--------------------------------------------------------------------------------

**Professeur :** Charles H. Pence  
**Horaire :** mercredi, 14h–16h, BARB 94 ou SUD 18 (regardez votre horaire)  
**Examen final :** (à déterminer)  
**Autres détails :** 4 crédits, 45.0h (en total, LSC 1120A+B)

## Contenu

Dans cette section du cours, nous explorons les questions suivantes : y a-t-il
une essence de l’humanité ? Les animaux non-humains sont-ils conscients? Quelle
est l’étendu de la cognition animale ? Quelles sont nos obligations morales
envers les animaux et la nature en général ?

## Partie générale

**À noter :** Ce site n’est destiné qu’aux étudiant·e·s dans le programme VETE
(LSC1120B). Ce cours comprend aussi une autre partie, LSC1120A, qui concerne les
étudiant·e·s VETE ainsi que les étudiant·e·s d’autres programmes. [Cliquez ici
pour le site de LSC1120A.]({{< relref "../lsc1120a" >}})

## Évaluation

L’évaluation prendra la forme d’un examen écrit, dans la session de janvier, la
session de juin, ainsi que la session d’août (100 %).

La note pour les étudiant·e·s inscrit·e·s dans tous les deux parties de LSC1120
sera composé de 50 % de cette note (partie VETE) et 50 % la note pour la partie
LSC1120A (partie générale).

## Enseignement

Ce cours sera normalement un cours magistral classique ; je présenterai les
concepts, problèmes, et questions d’interprétation classiques de la philosophie
et éthique des animaux. Je vous encourage de :

1.  Lire attentivement la lecture requise et, si vous avez le temps ou
    l’intérêt, peut-être voir quelques vidéos ou lire quelques « autres
    ressources » pour la session.
2.  Arriver au cours avec des questions ou points de clarification. Quels sont
    les points les plus intéressants ou les moins claires de la lecture ? Si ces
    question ne sont pas résolues pendant ma discussion, n’hésitez pas de les
    poser !
3.  Consultez les questions de révision (voir ci-dessous) régulièrement. Les
    questions de l’examen ne seront exactement les mêmes que cette liste, mais
    ils couvriront les mêmes sujets, donc si vous les révisez, vous serez déjà
    prêt pour l’examen.

<!-- **Certaines sessions de notre cours seront distribués par vidéo.** Ces vidéos
seront disponibles sur notre site Moodle. -->

## Lecture

Toute la lecture pour ce cours (y compris une portion des ressources facultatifs
ci-dessous) dans [mon propre site
« readings ».](https://readings.charlespence.net) Ce site est protégé par un mot
de passe, qui sera distribué par courriel et lors de notre première séance de
cours.

## Questions de révision

Ces questions sont préparées afin d’être utile pour vous dans votre révision de
la matière du cours. L’examen sera tiré de **la matière couverte** par ces
questions, mais **pas forcément dans exactement la même forme** que ces
questions. Elles indiquent alors ce que vous devez réviser, mais **il ne sera
pas utile du tout de mémoriser des réponses à ces questions.** Elles sont
divisées selon chacune de nos sept séances du cours.

[Télécharger les questions de révision
ici.](https://archive.charlespence.net/courses/lsc1120b/questions-revision.pdf)

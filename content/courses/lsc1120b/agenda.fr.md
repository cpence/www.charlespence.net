---
title: "LSC1120B : Agenda"
---

# LSC1120B : Agenda

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "agenda.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lsc1120b.php" class="button button-primary">Lecture</a>
<a href="https://archive.charlespence.net/courses/lsc1120b/questions-revision.pdf"
class="button button-primary">Questions de Révision</a>

--------------------------------------------------------------------------------

### 18 septembre : Introduction {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120b/semaine1.pdf)

#### Lecture obligatoire

Chapouthier, Georges. 2020. « Animal ». <http://encyclo-philo.fr/animal-a/>

#### Autres ressources

-   Vidéo : [Éthique animale: une
    introduction](https://www.youtube.com/watch?v=Ewspo-XUjVU)
-   Podcast : [Descartes et les
    animaux](https://www.canal-u.tv/video/ecole_normale_superieure_de_lyon/descartes_et_les_animaux.25667)
-   Vidéo : [Crash Course: Non-Human
    Animals](https://www.youtube.com/watch?v=y3-BX-jN_Ac)
-   Vidéo : [Peter Singer: Non-Human Animal
    Ethics](https://www.youtube.com/watch?v=TgRoZVT6kYc)
-   Afeissa, H.-S. éd. 2007. *Éthique animale.* Vrin
-   Armstrong, Susan J., and Richard G. Botzler, eds. 2008. *The Animal Ethics
    Reader.* 2nd edition. Oxon: Routledge.
-   Chapouthier, Georges. 2009. « Le respect de l’animal dans ses racines
    historiques : de l’animal-objet à l’animal sensible »
-   Feltz, Bernard. 2014. *La science et le vivant : Philosophie des sciences et
    modernité critique.* 2e édition revue et augmentée. Louvain-la-Neuve: De
    Boeck supérieur.
-   Ryan, Derek. 2015. *Animal Theory: A Critical Introduction.* Edinburgh:
    Edinburgh University Press.
-   Vilmer, Jean-Baptiste Jeangène. 2008. *Éthique animale.* PUF.

### 25 septembre : Nature humaine {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120b/semaine2.pdf)

#### Lecture obligatoire

Machery, Édouard. 2009 \[2008\]. « À propos de la notion de nature humaine ».

#### Autres ressources

-   [Human Nature](https://plato.stanford.edu/entries/human-nature/)
-   Hull, David. 1986. “On Human Nature”
-   Machery, Édouard. 2008. “A Plea for Human Nature” \[Original
    English-language version of the required reading\]
-   Ramsey, Grant. 2013. “Human Nature in a Post-Essentialist World”

### 2 octobre : Différences humain-animale {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120b/semaine3.pdf)

#### Lecture obligatoire

Chapouthier, Georges. 2010. « La spécificité de l’espèce humaine et sa
responsabilité à l’égard des autres espèces ».

#### Autres ressources

-   Vidéo : [Éthique animale: les similarités entre l’humain et les autres
    espèces](https://www.youtube.com/watch?v=FCH-YaMln-M)
-   Vidéo : [See What Happens When You Tickle a
    Rat](https://www.youtube.com/watch?v=d-84UJpYFRM)
-   Vidéo : [GAVAGAI: The Philosophical
    Thought-Experiment](https://www.youtube.com/watch?v=0YY4qxkqm3o%3E)
-   [Biological
    Information](https://plato.stanford.edu/entries/information-biological/)
-   Fitzpatrick, Simon. 2017. “Animal Morality: What is the Debate About?”

### 9 octobre : Cognition animale {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120b/semaine4.pdf)

#### Lecture obligatoire

de Waal, Frans. 2016. « Vagues cognitives ». Extrait de _Sommes-nous trop
« bêtes » pour comprendre l’intelligence des animaux ?_

ET/OU

Birch, Jonathan, et al. 2020. “Dimensions of Animal Consciousness”

#### Autres ressources

-   Vidéo : [Inside the Minds of
    Animals](https://www.youtube.com/watch?v=BDJ8xyQjyhM)
-   Vidéo : [Kristin Andrews: Understanding Cognition through
    Development](https://www.youtube.com/watch?v=qtD7kvIPi-o)
-   Vidéo : [Kristin Andrews: The “Other” Problems: Mind, Behavior and
    Agency](https://www.youtube.com/watch?v=khMoEjSb5RE)
-   [Animal Cognition](https://plato.stanford.edu/entries/cognition-animal/)
-   [Consciousness](https://plato.stanford.edu/entries/consciousness/)
-   [Animal
    Consciousness](https://plato.stanford.edu/entries/consciousness-animal/)
-   [Other Minds](https://plato.stanford.edu/entries/other-minds/)
-   Andrews, Kristin. 2015. *The Animal Mind: An Introduction to the Philosophy
    of Animal Cognition.* Oxon: Routledge.
-   Balcombe, Jonathan P. 2016. *What a fish knows: the inner lives of our
    underwater cousins.* First edition. New York: Scientific American/Farrar,
    Straus, and Giroux.
-   Block, Ned. 1995. “On a Confusion About a Function of Consciousness”
-   Block, Ned. 2005. “Two Neural Correlates of Consciousness”
-   Burghardt, Gordon M. 1985. “Animal Awareness”
-   Carruthers, Peter. 2008. “Meta-Cognition in Animals: A Skeptical Look”
-   Nagel, Thomas. 1974. “What is it like to be a bat?”
-   Russon, Anne and Andrews, Kristin. 2011. “Organgutan Pantomime: Elaborating
    the Message”

### 16 octobre : Obligations aux animaux {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120b/semaine5.pdf)

#### Lecture obligatoire

Regan, Tom. 1992 \[1985\]. « Pour les droits des animaux ».
<http://www.cahiers-antispecistes.org/pour-les-droits-des-animaux/>

#### Autres ressources

-   Vidéo : [Éthique animale: le
    spécisme](https://www.youtube.com/watch?v=UQylc0MmNaE)
-   Vidéo : [Éthique animale: élevage
    industriel](https://www.youtube.com/watch?v=vnBv0esL7Uo)
-   Vidéo : [Christine Korsgaard: Interacting with
    Animals](https://www.youtube.com/watch?v=27Hi04TwT8I)
-   [La responsabilité morale](http://encyclo-philo.fr/responsabilite-morale-a/)
-   [Spécisme](https://encyclo-philo.fr/specisme-gp/)
-   [The Moral Status of
    Animals](https://plato.stanford.edu/entries/moral-animal/)
-   [Moral
    Responsibility](https://plato.stanford.edu/entries/moral-responsibility/)
-   Giroux, Valéry. 2010. « Des droits légaux fondamentaux pour tous les êtres
    sensibles »
-   Haraway, Donna J. 2008. *When Species Meet.* Minneapolis: University of
    Minnesota Press.
-   Korsgaard, Christine M. 2005. “Fellow Creatures: Kantian Ethics and Our
    Duties to Animals.”
-   Korsgaard, Christine M. 2018. *Fellow Creatures: Our Obligations to the
    Other Animals.* Oxford: Oxford University Press.
-   Regan, Tom. 1986. “The Case for Animal Rights.” In Fox, M. W. and
    Mickley, I. D., eds., *Advances in Animal Welfare Science 1986/87,*
    pp. 179–189. Washington, D.C.: The Humane Society. \[Original
    English-language version of the required reading\]
-   Regan, Tom. 2004. *The Case for Animal Rights.* Berkeley: University of
    California Press.
-   Regan, Tom. 2010. « Le fondement moral du végétarisme »

### 23 octobre : Obligations à la nature {.syllabus-day}

#### [Slides](https://archive.charlespence.net/courses/lsc1120b/semaine6.pdf)

#### Lecture obligatoire

Callicott, J. Baird. 2007 \[1995\]. « Le valeur intrinsèque dans la nature : une
analyse métaéthique ».

#### Autres ressources

-   Vidéo : [Entretien avec J. Baird
    Callicott](https://www.youtube.com/watch?v=-l6GrcB0k6Y)
-   [Environmental
    Ethics](https://plato.stanford.edu/entries/ethics-environmental/)
-   Maris, Virginie. 2010. *Philosophie de la biodiversité.* Buchet/Chastel.
-   Rolston, Holmes III. 2007. “La valeur de la nature et la nature de la
    valeur”
-   Stone, Christopher D. 2007. “Le pluralisme moral et le développement de
    l’éthique environnementale”

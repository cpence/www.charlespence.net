---
title: "LFILO2602: Schedule"
---

# LFILO2602: Schedule

<a href="{{< relref "_index.en.md" >}}" class="button button-primary">General</a>
<a href="{{< relref "schedule.en.md" >}}" class="button button-primary">Schedule</a>
<a href="https://readings.charlespence.net/lfilo2602.php" class="button button-primary">Readings</a>
<a href="{{< relref "paper.en.md" >}}" class="button button-primary">Final Paper</a>

----------

### February 6: cancelled for lack of students {.syllabus-day}

### February 13: Introduction to Philosophy of Science {.syllabus-day}

_Readings:_ Carnap, "On the Character of Philosophic Problems" (1934), Popper,
"Science: Conjectures and Refutations" (1963)

### February 20: Opposing the Received View {.syllabus-day}

_Readings:_ Hanson, "Observation" (1958); Feyerabend, _Against Method,_ part 16
(1975; **optional**, but **recommended** no. 5, bottom p. 190 to bottom p. 197)

### February 27: Kuhn 1 {.syllabus-day}

_Readings:_ Kuhn, _Structure of Scientific Revolutions_ (1962), chapters 1–5
(version française, introduction–ch. 4)

### March 5: Kuhn 2 {.syllabus-day}

_Readings:_ Kuhn, _Structure of Scientific Revolutions_ (1962), chapters 6–9
(version française, ch. 5–8)

### March 12: Kuhn 3 {.syllabus-day}

_Readings:_ Kuhn, _Structure of Scientific Revolutions_ (1962), chapters 10–13
and Postscript (version française, ch. 9–12 et postface)

### March 19: Scientific Explanation {.syllabus-day}

_Readings:_ Hempel and Oppenheim, "Studies in the Logic of Explanation"
(excerpt, 1948); van Fraassen, "The Pragmatics of Explanation" (1977)

### March 26: Scientific Realism {.syllabus-day}

_Readings:_ Maxwell, "The Ontological Status of Theoretical Entities" (1962);
van Fraassen, "Arguments Concerning Scientific Realism" (1980); Laudan, "A
Confutation of Convergent Realism" (1981)

### April 16: Values in Science {.syllabus-day}

_Readings:_ Douglas, "Origins of the Value-Free Ideal for Science" (2009);
Douglas, "Values in Science" (2016; **optional** for more contemporary
background); Teller's letter to Szilard about their moral responsibility for the
atomic bomb (1945; **optional** but **recommended** and chilling)

### April 23: Feminist Philosophy of Science {.syllabus-day}

_Readings: Read one or both of:_ Richardson, "When Gender Criticism Becomes
Standard Practice: The Case of Sex Determination Genetics" (2008; for a
particuar application); Okruhlik, "Gender and the Biological Sciences" (1994;
for a broader overview)

### April 30: Scientific Objectivity {.syllabus-day}

_Readings:_ Daston and Galison, "The Image of Objectivity" (1992)

### May 7: Philosophy of Science in Practice {.syllabus-day}

_Readings:_ Chang, "Epistemic Activities and Systems of Practice" (2014)

### May 14: Student Presentations {.syllabus-day}

<!--

**If the number of students in the course permits it, we may condense student
presentations to a single day and have another day of material on May 7.**

-->

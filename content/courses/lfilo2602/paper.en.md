---
title: "LFILO2602: Paper Information"
---

# LFILO2602: Paper Information

<a href="{{< relref "_index.en.md" >}}" class="button button-primary">General</a>
<a href="{{< relref "schedule.en.md" >}}" class="button button-primary">Schedule</a>
<a href="https://readings.charlespence.net/lfilo2602.php" class="button button-primary">Readings</a>
<a href="{{< relref "paper.en.md" >}}" class="button button-primary">Final Paper</a>

----------

As noted above, the final paper will be written in stages. Here's more
information about how that process works. Note that all of the below (including
the paper as well as the presentation) may be in either French or English.

- **Outline/Sketch:** Due on **April 16.**
  <!-- Five weeks before the draft is due, or the last Sunday of spring break,
  if that falls during spring break -->

  Your first task is to prepare an outline or sketch of your paper idea. Your
  topic can be _anything that you are interested in,_ as long as it's in some
  way related to part of the material that we cover in the course. (If you're
  not sure, send me an e-mail to check.)

  There is no defined format for this outline, because I know that everyone has
  different ways in which they prepare papers. The minimal requirement is to
  have enough text that it would amount to around **one page total** (whether
  that's a "outline" or merely a sketch of what you would like to do is up to
  you), and to have **at least three sources** that will be important to your
  argument.

  Feel free as part of the outline to ask me for help -- whether that's finding
  more references, or help filling in parts of the argument, or anything else
  that you'd like.

  I will return comments on these outlines _by April 21._
  <!-- One week later, or Friday of the first full week back on campus after
  spring break -->

- **Draft for Comments:** Due **no later than May 12,** can be submitted earlier.
  <!-- End of week 12 -->

  If you would like comments on your paper from me, you must submit a full draft
  to me no later than May 12. This will give me enough time to write detailed
  comments, return them to you, and then give you enough time to be able to
  incorporate those comments.

  I will return comments on these drafts _by May 26._
  <!-- Two weeks after the above due date -->

- **Final Version:** Due **on June 15, by 23:59,** can be submitted earlier.
  <!-- As specified by the exam calendar; if they ask, I pick the Thursday of
  the second week of exam period -->

  I do not precisely specify the length required for this paper, but I would be
  surprised if you can write a high-quality, master's level research paper in
  less than 10 pages (1.5 spacing, 12pt normal font, etc.), and I would be
  equally surprised if you need more than 15 pages to make your point.

---
title: "LFILO2602 : Travail écrit"
slug: travail
---

# LFILO2602 : Détails sur le travail écrit

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "schedule.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lfilo2602.php" class="button button-primary">Lecture</a>
<a href="{{< relref "paper.fr.md" >}}" class="button button-primary">Travail</a>

-----


Comme indiqué ci-dessus, le travail final sera rédigé par étapes. Voici plus
d'informations sur le fonctionnement de ce processus. Notez que tous les devoirs
ci-dessous (y compris le travail et la présentation) peuvent être rédigés en
français ou en anglais.

- **Plan/Esquisse:** À remettre pour le **16 avril.**
  <!-- Five weeks before the draft is due, or the last Sunday of spring break,
  if that falls during spring break -->

  Votre première tâche consiste à préparer un plan ou une esquisse de votre idée
  de travail. Votre sujet peut être _n'importe quelle chose qui vous intéresse,_
  pourvu qu'elle soit liée d'une manière ou d'une autre à une partie du matériel
  que nous couvrons dans le cours. (Si vous n'êtes pas sûr, envoyez-moi un
  courriel pour vérifier).

  Il n'y a pas de format spécifique pour ce plan, car je sais que tout le monde
  a des méthodes différentes pour préparer des travaux. L'exigence minimale est
  d'avoir suffisamment de texte pour que cela représente environ **une page au
  total** (qu'il s'agisse d'un "plan" ou simplement d'une esquisse de ce que
  vous souhaitez faire, c'est à vous de voir), et d'avoir **au moins trois
  sources** qui seront importantes pour votre argumentation.

  N'hésitez pas, dans le cadre du plan, à me demander de l'aide, qu'il s'agisse
  de trouver d'autres références, de compléter des parties de l'argumentation ou
  de tout autre élément que vous souhaitez.

  Je renverrai des commentaires sur ces plans _avant le 21 avril._
  <!-- One week later, or Friday of the first full week back on campus after
  spring break -->

- **Ébauche à commenter :** À remettre pour **le 12 mai** (ou plus tôt).
  <!-- End of week 12 -->

  Si vous souhaitez que je vous donne des commentaires sur votre travail, vous
  devez me soumettre une ébauche complète au plus tard le 7 mai. Cela me
  laissera suffisamment de temps pour rédiger des commentaires détaillés, vous
  les renvoyer, puis vous donner suffisamment de temps pour pouvoir intégrer ces
  commentaires.

  Je renverrai des commentaires sur ces ébauches _avant le 26 mai._
  <!-- Two weeks after the above due date -->

- **Version finale :** À remettre pour **le 15 juin, 23h59** (ou plus tôt).
  <!-- Thursday of the second week of exam period -->

  Je ne spécifie pas précisément la longueur requise pour ce travail, mais je
  serais surpris que vous puissiez écrire un document de recherche de qualité,
  de niveau master, en moins de 10 (interligne 1,5, police 12pt normale, etc.),
  et je serais également surpris que vous ayez besoin de plus de 15 pages pour
  exprimer votre argument.

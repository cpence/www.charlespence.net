---
title: "LFILO2602 : Philosophie des sciences (questions approfondies)"
---

# LFILO2602 : Philosophie des sciences (questions approfondies)

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "schedule.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lfilo2602.php" class="button button-primary">Lecture</a>
<a href="{{< relref "paper.fr.md" >}}" class="button button-primary">Travail</a>

_Site web pour 2Q, 2023-2024 ; dernière mise à jour le 29 août, 2023_

---

**Professeur :** Charles H. Pence  
**Horaire :** mardi, 10h45-12h45, SOCR 43  
**Autres détails :** 5.0 crédits, 30.0h

### Thèmes abordés

Chaque année, trois thèmes centraux sont abordés touchant au dialogue entre les
sciences naturelles et les questionnements philosophiques.

### Contenu

Ce cours prendra la forme d'une « vue d'ensemble avancée » sur la philosophie
des sciences, conçu pour permettre à l'étudiant·e de poursuivre des études de
haut niveau ou de préparer un mémoire de master sur des sujets spécifiques. Nous
commencerons par un bref aperçu historique de la philosophie des sciences, puis
nous examinerons un certain nombre de problèmes en la philosophie des sciences,
tels que le débat sur l'explication scientifique, la dispute entre les réalistes
et les anti-réalistes scientifiques, et des questions sur la relation entre la
science et l'éthique.

### Autres informations

Ce cours alterne entre le français et l'anglais. Pour Q2 2023--2024, il sera
enseigné **en français.**

Pour tous les deux versions du cours, le cours exige une connaissance importante
de l'anglais ; nous lirons des sources de la philosophie des sciences qui ne
sont souvent pas disponibles en traduction. Les étudiant·e·s sont également
libres de poser des questions et de discuter du matériel de cours avec moi en
toutes les deux langues. Tous les devoirs du cours peuvent être rédigés aussi en
anglais ou en français. (Quand c’est enseigné en français, ce cours est
formellement désigné comme “English-Friendly.”)

## Lecture obligatoire

Tout de la lecture sera distribué électroniquement [dans le site web
« readings ».](http://readings.charlespence.net) Le mot de passe pour ce site
sera distribué par courriel avant le début du cours et pendant la première
séance.

Nous allons lire l'intégralité de _Structure des révolutions scientifiques_ de
Kuhn. Je posterai une copie numérique pour que vous puissiez l'utiliser si vous
le souhaitez, mais je pense qu'il vaut mieux de posséder le vôtre. Il est, bien
sûr, disponible en traduction française.

## Évaluation

- **Travail final écrit (60 %) :** Le principal résultat de ce cours sera un
  seul travail écrit. Vous êtes libre de rédiger ce travail en anglais ou en
  français, mais si vous l'écrivez en français, je ne serai pas en mesure de
  fournir des commentaires sur le style ou la qualité de vos écrits
  universitaires.

  Nous construirons cet article par étapes, en commençant par un bref aperçu à
  rendre vers le milieu du quadrimestre, suivi d'une ébauche sur laquelle je
  ferai des commentaires et d'une version finale à la fin du quadri, que vous
  présenterez lors des présentations orales. L'objectif est de produire des
  articles de grande qualité, susceptibles d'être soumis à une revue spécialisée
  ou à un congrès, ou encore de servir d'exemples d'écriture pour votre entrée
  dans un programme de doctorat, si vous en avez envie.

  Certains sujets de travaux seront discutés au cours du semestre, mais il vous
  incombera en fin de compte de choisir un sujet correspondant à vos intérêts.
  _Les étudiants qui choisissent un sujet qu'ils aiment vraiment obtiennent
  presque toujours de meilleures notes._ Prenez le temps de réfléchir (et de me
  parler !) à la façon de relier notre matière à vos divers intérêts
  philosophiques.

- **Présentation orale (40%):** À la fin du cours, il vous sera demandé de
  présenter le sujet de votre projet final, ainsi que de commenter les
  présentations de vos collègues.

Dans la session d'août, l'évaluation consiste d'un examen écrit (100 %).

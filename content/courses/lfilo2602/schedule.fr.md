---
title: "LFILO2602 : Agenda"
slug: agenda
---

# LFILO2602 : Agenda

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "schedule.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lfilo2602.php" class="button button-primary">Lecture</a>
<a href="{{< relref "paper.fr.md" >}}" class="button button-primary">Travail</a>

----------

### 6 février : annulé faute d’étudiant·e·s {.syllabus-day}

### [13 février : Introduction à la philosophie des sciences](https://archive.charlespence.net/courses/lfilo2602/semaine1.pdf) {.syllabus-day}

_Lecture :_ Carnap, "On the Character of Philosophic Problems" (1934) ; Popper,
"Science: Conjectures and Refutations" (1963)

### [20 février : Contre l'idée reçu de la science](https://archive.charlespence.net/courses/lfilo2602/semaine2.pdf) {.syllabus-day}

_Lecture :_ Hanson, "Observation" (1958) ; Feyerabend, _Against Method,_ pt. 16
(1975 ; **facultative**, mais **recommandée** no. 5, fin p. 190-fin p. 197)

### [27 février : Kuhn 1](https://archive.charlespence.net/courses/lfilo2602/semaine3.pdf) {.syllabus-day}

_Lecture :_ Kuhn, _La structure des révolutions scientifiques_ (1962),
introduction-ch. 4 (version anglaise, ch. 1-5)

### [5 mars : Kuhn 2](https://archive.charlespence.net/courses/lfilo2602/semaine4.pdf) {.syllabus-day}

_Lecture :_ Kuhn, _La structure des révolutions scientifiques_ (1962), ch. 5-8
(version anglaise, ch. 6-9)

### [12 mars : Kuhn 3](https://archive.charlespence.net/courses/lfilo2602/semaine5.pdf) {.syllabus-day}

_Lecture :_ Kuhn, _La structure des révolutions scientifiques_ (1962), ch. 9-12
et postface (version anglaise, ch. 10-13 et postscript)

### [19 mars : L'explication scientifique](https://archive.charlespence.net/courses/lfilo2602/semaine6.pdf) {.syllabus-day}

_Lecture :_ Hempel and Oppenheim, "Studies in the Logic of Explanation"
(extrait, 1948) ; van Fraassen, "The Pragmatics of Explanation" (1977)

### [26 mars : Le réalisme scientifique](https://archive.charlespence.net/courses/lfilo2602/semaine7.pdf) {.syllabus-day}

_Lecture :_ Maxwell, "The Ontological Status of Theoretical Entities" (1962) ;
van Fraassen, "Arguments Concerning Scientific Realism" (1980) ; Laudan, "A
Confutation of Convergent Realism" (1981)

### [16 avril : Les valeurs dans la science](https://archive.charlespence.net/courses/lfilo2602/semaine8.pdf) {.syllabus-day}

_Lecture :_ Douglas, "Origins of the Value-Free Ideal for Science" (2009) ;
Douglas, "Values in Science" (2016 ; **facultative** pour plus de contexte
contemporain) ; la lettre de Teller à Szilard sur leurs obligations morales pour
la bombe atomique (1945 ; **facultative** mais **recommandée** et effrayante)

### [23 avril : Philosophie féministe de la science](https://archive.charlespence.net/courses/lfilo2602/semaine9.pdf) {.syllabus-day}

_Lecture : Une ou les deux de :_ Richardson, "When Gender Criticism Becomes
Standard Practice: The Case of Sex Determination Genetics" (2008 ; pour un cas
d'étude particulier) ; Okruhlik, "Gender and the Biological Sciences" (1994 ;
pour une vue d'ensemble)

### [30 avril : L'objectivité scientifique](https://archive.charlespence.net/courses/lfilo2602/semaine10.pdf) {.syllabus-day}

_Lecture :_ Daston et Galison, "The Image of Objectivity" (1992)

### 7 mai : Philosophie des sciences et la pratique {.syllabus-day}

_Lecture :_ Chang, "Epistemic Activities and Systems of Practice" (2014)

### 14 mai : Présentations étudiantes {.syllabus-day}

<!--

**Si le nombre d'étudiant·e·s dans le cours le permet, on peut condenser les
présentations à un seul jour et ajouter un jour de matière le 9 mai.**

-->

---
title: "LFILO2602: Philosophy of Science (Advanced Studies)"
---

# LFILO2602: Philosophy of Science (Advanced Studies)

<a href="{{< relref "_index.en.md" >}}" class="button button-primary">General</a>
<a href="{{< relref "schedule.en.md" >}}" class="button button-primary">Schedule</a>
<a href="https://readings.charlespence.net/lfilo2602.php" class="button button-primary">Readings</a>
<a href="{{< relref "paper.en.md" >}}" class="button button-primary">Final Paper</a>

_Website for 2Q, 2023–2024; last updated on August 29, 2023_

---

**Professor:** Charles H. Pence  
**Course Time:** Tuesday, 10h45–12h45, SOCR 43  
**Course Details:** 5.0 credits, 30.0h

### Main Themes

Each year, three central themes are addressed concerning the dialogue between
natural sciences and philosophical questions.

### Content

This course will take the form of an advanced survey of philosophy of science,
designed to allow the student to pursue further high-level study or prepare a
master's mémoire on specific topics. We will start with a brief historical
overview of the philosophy of science, then consider a number of problems in
philosophy of science, such as the debate over scientific explanation, the
dispute between scientific realists and anti-realists, and questions about the
relationship between science and ethics.

### Other Information

This course alternates every year between being taught in French and in English.
For Q2, 2022--23, it will be taught **in English.**

For both versions, the course requires a significant knowledge of English; we
will be reading sources from the philosophy of science that are often
unavailable in translation. Students are always free to ask questions and
discuss the course material with me in either language, and all course
assignments may be written in either language. (When taught in English, this
course is officially designated as “French-Friendly.”)

## Required Readings

All of the readings will be posted electronically [on the course readings
website.](http://readings.charlespence.net) The password for this website will
be distributed via e-mail before the start of the course and in the first class
session.

We will read the entirety of Kuhn's _Structure of Scientific Revolutions._ I
will post a digital copy for you to use if you want, but I do think it's worth
owning your own. It is, of course, available in French translation as well, and
you are welcome to read it in French.

## Assignments and Grading

- **Final Paper (60%):** The primary output from this course will be a single
  seminar paper. You are free to write this paper in English or in French,
  though if you write in French I will not be able to provide commentary on the
  style or quality of your academic writing.

  We will construct this paper in stages, beginning with a short outline due
  around the middle of the quadrimester, followed by a draft on which I will
  offer comments and a final draft at the end of the semester, which you will
  present in your oral presentation. The hope is to produce high-quality papers,
  suitable for submission to a graduate journal, a conference, or as writing
  samples for your entrance into a doctoral program, should you be inclined to
  do so.

  Some paper topics will be discussed over the course of the semester, but it
  will ultimately be your responsibility to select a topic in line with your
  interests. _Students who select a paper topic that they genuinely enjoy almost
  always earn higher grades._ Spend time thinking (and talking to me!) about how
  to connect our material to your various philosophical interests.

- **Oral Presentation (40%):** At the end of the course, you will be asked to
  present the topic of your final paper project, as well as to provide
  commentary on the presentations of your fellow students.

In the August session, the evaluation consists of a written exam (100%).

---
title: "LFILO1220 : Philosophie des sciences"
---

# LFILO1220 : Philosophie des sciences

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "agenda.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lfilo1220.php" class="button button-primary">Lecture</a>

*Site web pour 2Q, 2024-2025 ; dernière mise à jour le 3 février 2025*

--------------------------------------------------------------------------------

**Professeur :** Charles H. Pence  
**Horaire :** mardi, 16h15-18h15, ERAS 58 **et** mercredi, 10h45-11h45, SOCR 23  
**Date de l’examen final :** 2 juin, 8h30-10h30, ERAS 54  
**Autres détails :** 5.0 crédits, 45.0h

### Thèmes abordés

L’objectif de ce cours est de donner aux étudiants une première initiation à la
philosophie des sciences contemporaine. Une attention particulière sera accordée
à la mise en évidence des convergences, mais aussi des différences, entre la
philosophie des sciences de la matière, la philosophie des sciences du vivant et
la philosophie des sciences humaines et sociales. Sera aussi abordé le problème
de l’articulation entre les technosciences et la société, incluant l’importante
question de l’écologie.

Parmi les thèmes transversaux abordés en philosophie des sciences, on
retrouvera : le statut épistémique des théories et des modèles scientifiques, la
dynamique de la science, la portée et les limites des savoirs scientifiques, la
théorie de l’explication scientifique, le réductionnisme, le rôle de la
finalité, le naturalisme et la question de l’objectivité ou de la neutralité
axiologique. Parmi les thèmes transversaux abordés en science-société, on
trouvera : science-modernité-religion et sciences-expertise-écologie.

### Acquis d’apprentissage

Au terme du cours, l’étudiant devra maîtriser les grandes problématiques et les
principaux auteurs de la philosophie des sciences. Il devra être capable de
présenter oralement ou par écrit, d’une façon claire, synthétique et précise une
question. Il pourra argumenter de façon rigoureuse en faveur des thèses abordées
et sera capable de prendre un recul critique par rapport à celles-ci. Il sera
capable de s’orienter dans la littérature en philosophie des sciences.

### Contenu

Pourquoi la science a-t-elle une place privilégiée dans le discours
contemporain ? Normalement, la réponse à cette question est censée impliquer la
nature et la construction de la connaissance scientifique. Mais comment
fonctionne cette connaissance ? Dans l’histoire de la science, comment a-t-elle
été véritablement construite ? Y a-t-il une « méthode scientifique » ? La
science procède-t-elle par des « révolutions » ou des « changements de
paradigme » ? Qu’est-ce que la relation entre la science et les valeurs
éthiques ? La science d’aujourd’hui a-t-elle changé de caractère par rapport à
la science de Newton, Darwin, ou d’Einstein ?

Dans ce cours, on abordera ces questions (et d’autres), analysant la nature
épistémique de la connaissance scientifique en dialogue entre l’histoire et la
philosophie des sciences. Autrement dit, on discutera en même temps ce que les
scientifiques font (en vrai, au-delà des caricatures) et comment le comprendre.

## Lecture

Tout de la lecture sera distribué électroniquement [dans le site web
« readings ».](http://readings.charlespence.net) Le mot de passe pour ce site
sera distribué par courriel avant le début du cours et pendant la première
séance.

**Regardez** ce programme chaque fois avant de lire : vous trouvez que souvent
il y a des extraits ou des parties facultatives dans chaque lecture.

## Évaluation

L’évaluation pour ce cours procéderont « à l’américaine » : la plupart est
consacré à deux examens qui couvrent chacun la moitié du cours (c’est-à-dire que
l’examen final *n’est pas* cumulatif).

-   **Quiz préparatoire (5 %) :** Ce quiz est construit simplement pour vous
    donner une idée du format des deux examens. Il ne fait qu’un partie assez
    minime de votre note finale, et vous permettra d’« essayer » deux questions
    du même type qui figurent sur les examens.

-   **Examen mi-parcours (35 %) :** Le premier examen comprendra la matière
    abordée dans la première moitié du cours, et aura lieu lors d’une session de
    notre cours.

-   **Examen final (45 %) :** Le deuxième examen (pendant la session d’examens
    de juin) comprendra la matière de la deuxième moitié du cours.

-   **Assistance au cours, participation aux TP (15 %) :** C’est crucial que
    vous assistez au cours, y compris nos « TPs », afin de pouvoir discuter de
    notre sujet avec les autres étudiant·e·s. Ça fera alors partie de votre
    note.

Dans la deuxième session, tous ces parties seront remplacés par **un seul examen
écrit (100 %).** Je recommande *fortement* que vous suivez le cours et le
terminez dans la session de juin.

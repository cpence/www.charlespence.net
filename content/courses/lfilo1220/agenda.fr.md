---
title: "LFILO1220 : Agenda"
---

# LFILO1220 : Agenda

<a href="{{< relref "_index.fr.md" >}}" class="button button-primary">Général</a>
<a href="{{< relref "agenda.fr.md" >}}" class="button button-primary">Agenda</a>
<a href="https://readings.charlespence.net/lfilo1220.php" class="button button-primary">Lecture</a>

--------------------------------------------------------------------------------

#### [4 février : Introduction à la philosophie des sciences](https://archive.charlespence.net/courses/lfilo1220/seance1.pdf) {.syllabus-day}

*Lecture :* Bachelard, *Le nouvel esprit scientifique,* « La complexité
essentielle de la philosophie scientifique », s.I-II (s.III facultative)

## Partie I : Les révolutions scientifiques

#### [5 février : Qu’est-ce qu’une révolution scientifique ?](https://archive.charlespence.net/courses/lfilo1220/seance2.pdf) {.syllabus-day}

*Lecture :* Kuhn, *La structure des révolutions scientifiques* (extraits)

#### [11 février : **La** Révolution Scientifique](https://archive.charlespence.net/courses/lfilo1220/seance3.pdf) {.syllabus-day}

*Lecture :* Shapin, *La révolution scientifique,* ch. 1 (pp. 31-47 obligatoire,
le reste facultatif)

#### [12 février : La science médiévale : La Révolution Scientifique existait-elle ?](https://archive.charlespence.net/courses/lfilo1220/seance4.pdf) {.syllabus-day}

*Lecture :* Duhem, *Le système du monde,* « La dynamique des Hellènes après
Aristote » (extrait)

#### [18 février : La science en Chine et le monde arabe](https://archive.charlespence.net/courses/lfilo1220/seance5.pdf) {.syllabus-day}

*Lecture :* Bonnet-Bidaud, « La science chinoise » **et** Weinberg, « L’apport
 du monde arabe »

#### 19 février : **\[JOURNÉE DE DISCUSSION\]** {.syllabus-day}

#### [25 février : Lavoisier : la science, l’argent, et la (l’autre) révolution](https://archive.charlespence.net/courses/lfilo1220/seance7.pdf) {.syllabus-day}

*Lecture :* Bensaude-Vincent & Stengers, *Histoire de la chimie* (extrait)

#### [26 février : Lamarck et le « transformisme » avant Darwin](https://archive.charlespence.net/courses/lfilo1220/seance8.pdf) {.syllabus-day}

*Lecture :* Lamarck, *Philosophie zoologique* (extraits)

#### 4 mars et 5 mars: *pas de cours, Pr. Pence indisponible* {.syllabus-day}

**Envoi du quiz préparatoire** (4 mars, même sans cours)

#### [7 mars (vendredi !), 10h45-12h45, ERAS 58 : La géométrie non euclidienne, la perception, et Kant](https://archive.charlespence.net/courses/lfilo1220/seance11.pdf)

**N.B. : ce cours sera enregistré pour celles et ceux qui ne sont pas
disponible à cette heure**

*Lecture :* Besnier, « Le statut de l’espace dans la *Critique de la raison
pure* de Kant »

#### [11 mars : La révolution darwinienne](https://archive.charlespence.net/courses/lfilo1220/seance9.pdf) {.syllabus-day}

*Lecture :* Darwin, *L’origine des espèces* (extraits)

#### 12 mars : **\[TP 1\]** Darwin et les naturalistes {.syllabus-day}

*Lecture :* Support pour le TP (site readings)  
**Soumission du quiz préparatoire**

#### 18 mars : TP 2 et Einstein et l’espace-temps (hybride), A.04B SCES {.syllabus-day}

**Notez bien l’auditoire qui n’est pas celle d’habitude !**

- **\[TP 2\]** Lemaître et le Big Bang
- [Einstein et l’espace-temps](https://archive.charlespence.net/courses/lfilo1220/seance12.pdf)

  *Lecture :* Bachelard, *Le nouvel esprit scientifique,* « La mécanique
  non-newtonienne », s.I-IV (s.V facultative)

#### [19 mars : L’anarchisme méthodologique](https://archive.charlespence.net/courses/lfilo1220/seance14.pdf) {.syllabus-day}

*Lecture :* Feyerabend, *Contre la méthode,* ch. 16 (de numéro 5, p. 282 à la
rupture, p. 292, reste facultatif)

#### 25 mars : **\[JOURNÉE DE DISCUSSION\]** {.syllabus-day}

#### 26 mars : **\[EXAMEN MI-PARCOURS\]** {.syllabus-day}

**sur la Partie I uniquement**

## Partie II : La sciences et les valeurs éthiques

#### [1 avril : Théorie philosophique : science et valeur éthique](https://archive.charlespence.net/courses/lfilo1220/seance17.pdf) {.syllabus-day}

*Lecture :* Coutellec, « Une réévaluation du pluralisme axiologique »

#### [2 avril : Le déni de la science, changement climatique, agnotologie](https://archive.charlespence.net/courses/lfilo1220/seance18.pdf) {.syllabus-day}

*Lecture :* Oreskes et Conway, *Les marchands de doute* (ch. 6, pp. 208-303 et
348-352 obligatoire, reste facultatif)

#### [8 avril : La science contre le racisme](https://archive.charlespence.net/courses/lfilo1220/seance19.pdf) {.syllabus-day}

*Lecture :* Gould, *La mal-mesure de l’homme* (ch. 5, pp. 278-295 facultatif,
pp. 296-313 obligatoire)

#### 9 avril : **\[JOURNÉE DE DISCUSSION\]** {.syllabus-day}

## Partie III : La génération de la connaissance scientifique

#### [15 avril : Méthode expérimentale, empirisme, et Galilée](https://archive.charlespence.net/courses/lfilo1220/seance21.pdf) {.syllabus-day}

*Lecture :* Galileo, *Discours concernant deux sciences nouvelles* (extraits)

#### [16 avril : Modélisation et simulation scientifique](https://archive.charlespence.net/courses/lfilo1220/seance22.pdf) {.syllabus-day}

*Lecture :* Varenne, *Théorie, réalité, modèle,* « Modèle et réalité »

#### [6 mai : **\[TP 3\]** Les expériences de Galileo](https://archive.charlespence.net/courses/lfilo1220/seance23.pdf) {.syllabus-day}

#### [7 mai : Sciences historiques et expérimentales](https://archive.charlespence.net/courses/lfilo1220/seance24.pdf) {.syllabus-day}

*Lecture :* Gayon, « De la biologie comme science historique »

#### [13 mai : La science et la pseudo-science](https://archive.charlespence.net/courses/lfilo1220/seance25.pdf) {.syllabus-day}

*Lecture :* Debray, « Pseudoscience »

#### 14 mai : **\[DISCUSSION FINALE\]** {.syllabus-day}

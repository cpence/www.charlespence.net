---
title: Infos au hasard
---

# Infos au hasard

## Réseaux sociaux

- [Mastodon (préféré)](https://scholar.social/@pence)
- [Codeberg (logiciels open-source)](https://codeberg.org/cpence)
- (_autres supprimés_)

## Course à pied

- [Smashrun (entrainement)](http://smashrun.com/cpence)
- [Athlinks (courses)](http://athlinks.com/racer/130406384)

## Généalogie académique

Mon promoteur a été [Grant Ramsey](http://theramseylab.org). Il a eu deux
co-promoteurs, [Alex Rosenberg](http://www.duke.edu/~alexrose) et [Robert
Brandon](http://fds.duke.edu/db/aas/Philosophy/rbrandon). Je suis donc de la
même famille académique de [W.V.O.
Quine](http://en.wikipedia.org/wiki/Willard_Van_Orman_Quine)
(arrière²-grand-père), [Hans
Reichenbach](http://en.wikipedia.org/wiki/Hans_Reichenbach)
(arrière²-grand-père), [Alfred North
Whitehead](http://en.wikipedia.org/wiki/Alfred_North_Whitehead)
(arrière³-grand-père), [Nicolaus
Copernicus](http://en.wikipedia.org/wiki/Nicolaus_Copernicus)
(arrière¹⁹-grand-père), et [Marsilio
Ficino](http://en.wikipedia.org/wiki/Marsilio_Ficino) (arrière²⁶-grand-père),
entre autres.

## Numéro Erdös

Mon numéro Erdös est au maximum 6. J’ai publié [avec Holly
Goodson](https://doi.org/10.1093/bioinformatics/btr684), qui a publié [avec Mark
S. Alber](https://doi.org/10.1103/PhysRevE.74.041920), qui a publié [avec Marcin
Sikora](https://doi.org/10.1109/TIT.2009.2037051), qui a publié [avec Daniel J.
Costello, Jr.](https://doi.org/10.1109/TIT.2006.874520), qui a publié [avec
Peter C. Fishburn](https://doi.org/10.1109/18.370145), qui a publié [avec Paul
Erdös](https://doi.org/10.1137/0404030).

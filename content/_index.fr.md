---
title: Accueil
---

<!--
{{< news >}}

### Excellentes nouvelles !

J'ai été élu à un poste de quatre ans dans le Collegium de l'Académie royale de Belgique ! Restez à l'affût des nouveaux projets et collaborations.

{{< /news >}}
-->

# À propos

<img src="/images/pence.png" alt="Charles Pence" class="u-pull-right" style="max-width: 150px; margin: 1rem;">

Je suis philosophe et historien des sciences, et Chargé de cours (équiv. MCF) à
[l'Institut supérieur de
philosophie](https://uclouvain.be/fr/instituts-recherche/isp), le [Louvain
Institute of Biomolecular Science and
Technology](https://uclouvain.be/en/research-institutes/libst), et à la [Faculté
de philosophie, arts et lettres](https://uclouvain.be/fr/facultes/fial) à
[l'Université catholique de Louvain](https://uclouvain.be) à Louvain-la-Neuve,
en Belgique.

Je suis le directeur du [Centre de philosophie des sciences et sociétés
(CEFISES)](https://cefises.be/fr). Je suis aussi un membre du [Collegium de
l’Académie royale de
Belgique](https://academieroyale.be/fr/l-academie-royale-collegium-presentation-bureau/),
et le rédacteur en chef de la revue [_Philosophy, Theory, and Practice in
Biology_](https://www.ptpbio.org/).

J’ai obtenu un bachelier de philosophie à l’Université de Princeton et un
doctorat en histoire et philosophie des sciences à l’Université de Notre Dame.
Mes publications ont paru dans des presses universitaires et revues
scientifiques éminentes, et j’ai présenté mes recherches dans de nombreuses
conférences prestigieuses aux quatre coins du monde. [Il est possible de
télécharger une copie de mon CV ici,](https://archive.charlespence.net/cv.pdf)
et de me contacter par mail à l'adresse <charles@charlespence.net>.

Quand je ne travaille pas (ce qui est plutôt rare), je suis un marathonien avide
mais aussi un technologue et un audiophile. Pour plus d’infos sur moi, [cliquez
ici…]({{< relref "random.md" >}})

{{< leftcol >}}

## Mes recherches

Je dirige une équipe de chercheurs dévoués, dont le travail est de comprendre la
philosophie, l’histoire, et l’éthique des sciences et des technologies. Et nous
cherchons encore et toujours d’autres collaborateur·rice·s ! Vous pouvez visiter
le site web du laboratoire pour en apprendre davantage sur nos recherches.

<a href="https://pencelab.be/fr" class="button button-primary">En savoir
plus…</a>

{{< /leftcol >}}

{{< rightcol >}}

## Mon enseignement

J’enseigne une grande variété de cours à UCLouvain, à destination d’étudiant·e·s
qui viennent de différentes disciplines. Si vous êtes l’un·e de mes
étudiant·e·s, ou si vous êtes simplement intéressé·e par les sujets que
j'enseigne, vous pouvez cliquer ci-dessous afin d'en savoir plus sur ce que je
suis en train d’enseigner en ce moment et ce que j’ai prévu d’enseigner à
l’avenir.

<a href="{{< ref "courses" >}}" class="button button-primary">Mes cours…</a>

{{< /rightcol >}}


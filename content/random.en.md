---
title: Random Stuff
---

# Random Stuff

## Social Media

- [Mastodon (preferred)](https://scholar.social/@pence)
- [Codeberg (open-source software)](https://codeberg.org/cpence)
- (_others deleted_)

## Running

- [Smashrun (training)](http://smashrun.com/cpence)
- [Athlinks (races)](http://athlinks.com/racer/130406384)

## Academic Genealogy

My supervisor was [Grant Ramsey](http://theramseylab.org). He had two
co-supervisors, [Alex Rosenberg](http://www.duke.edu/~alexrose) and [Robert
Brandon](http://fds.duke.edu/db/aas/Philosophy/rbrandon). I'm thus in the same
academic family as [W.V.O.
Quine](http://en.wikipedia.org/wiki/Willard_Van_Orman_Quine)
(great²-grandfather), [Hans
Reichenbach](http://en.wikipedia.org/wiki/Hans_Reichenbach)
(great²-grandfather), [Alfred North
Whitehead](http://en.wikipedia.org/wiki/Alfred_North_Whitehead)
(great³-grandfather), [Nicolaus
Copernicus](http://en.wikipedia.org/wiki/Nicolaus_Copernicus)
(great¹⁹-grandfather), and [Marsilio
Ficino](http://en.wikipedia.org/wiki/Marsilio_Ficino) (great²⁶-grandfather),
among others.

## Erdös Number

My Erdös number is at most 6. I've pubished [with Holly
Goodson](https://doi.org/10.1093/bioinformatics/btr684), who published [with
Mark S. Alber](https://doi.org/10.1103/PhysRevE.74.041920), who published [with
Marcin Sikora](https://doi.org/10.1109/TIT.2009.2037051), who published [with
Daniel J. Costello, Jr.](https://doi.org/10.1109/TIT.2006.874520), who published
[with Peter C. Fishburn](https://doi.org/10.1109/18.370145), who published [with
Paul Erdös](https://doi.org/10.1137/0404030).

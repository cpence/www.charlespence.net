# charlespence.net

These are the sources from which I build my personal website. Not much really
exciting; it's basically just a multilingual Hugo site, with a slightly fancy
way to make the course websites from a Hugo section.

## License

Copyright (C) 2018–2021, Charles H. Pence.

**Attribution 4.0 International (CC BY 4.0).** This is a human-readable summary
of (and not a substitute for) the license. You are free to:

*   Share — copy and redistribute the material in any medium or format
*   Adapt — remix, transform, and build upon the material for any purpose, even
    commercially.

This license is acceptable for Free Cultural Works. The licensor cannot revoke
these freedoms as long as you follow the license terms.

For more information, see the `LICENSE` file.
